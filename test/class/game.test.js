const Game = require('../../class/game');

test('test game load', () => {
  const example = [
    [1, 2],
    [10, null],
    [5, 4],
    [7, 3],
    [10, null],
    [10, null],
    [1, 4],
    [6, 2],
    [7, 3],
    [10, 3, 7]
  ];

  let game = new Game();
  game.load(example);

  expect(game.getFrame(0).getScore()).toBe(3);
  expect(game.getFrame(1).getScore()).toBe(19);
  expect(game.getFrame(2).getScore()).toBe(9);
  expect(game.getFrame(3).getScore()).toBe(20);
  expect(game.getFrame(4).getScore()).toBe(21);
  expect(game.getFrame(5).getScore()).toBe(15);
  expect(game.getFrame(6).getScore()).toBe(5);
  expect(game.getFrame(7).getScore()).toBe(8);
  expect(game.getFrame(8).getScore()).toBe(20);
  expect(game.getFrame(9).getScore()).toBe(20);

  expect(game.getScore()).toBe(140);
});

test('test perfect match', () => {
  const example = [
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, null],
    [10, 10, 10]
  ];

  let game = new Game();
  game.load(example);

  expect(game.getFrame(0).getScore()).toBe(30);
  expect(game.getFrame(1).getScore()).toBe(30);
  expect(game.getFrame(2).getScore()).toBe(30);
  expect(game.getFrame(3).getScore()).toBe(30);
  expect(game.getFrame(4).getScore()).toBe(30);
  expect(game.getFrame(5).getScore()).toBe(30);
  expect(game.getFrame(6).getScore()).toBe(30);
  expect(game.getFrame(7).getScore()).toBe(30);
  expect(game.getFrame(8).getScore()).toBe(30);
  expect(game.getFrame(9).getScore()).toBe(30);

  expect(game.getScore()).toBe(300);
});