const Frame = require('../../class/frame');

//----------------------------------------------------
test('test frame load', () => {
  let frame = new Frame();
      frame.load([1,2]);

  expect(frame.getFirstRoll()).toBe(1);
  expect(frame.getSecondRoll()).toBe(2);
  expect(frame.getScore()).toBe(3);
});

//----------------------------------------------------
test('test frame extra roll load', () => {
  let frame = new Frame();
      frame.load([1, 2, 3]);

  expect(frame.getFirstRoll()).toBe(1);
  expect(frame.getSecondRoll()).toBe(2);
  expect(frame.getScore()).toBe(6);
});

//----------------------------------------------------
test('test isStrike', () => {
  let frame1 = new Frame();
      frame1.load([7, 3]);

  expect(frame1.isStrike()).toBe(false);

  let frame2 = new Frame();
      frame2.load([10, null]);

  expect(frame2.isStrike()).toBe(true);
});

//----------------------------------------------------
test('test isSpare', () => {
  let frame1 = new Frame();
      frame1.load([7, 2]);

  expect(frame1.isSpare()).toBe(false);

  let frame2 = new Frame();
      frame2.load([7, 3]);

  expect(frame2.isSpare()).toBe(true);
});

//----------------------------------------------------
test('test oneFrameForward', () => {
  let frame1 = new Frame();
  let frame2 = new Frame();

  frame1.load([7, 3]);
  frame2.load([6, 3]);

  frame1.oneFrameForward(frame2);
  expect(frame1.getScore()).toBe(16);

  frame1.load([10, null]);
  frame2.load([6, 4]);

  frame1.oneFrameForward(frame2);
  expect(frame1.getScore()).toBe(20);

  frame1.load([10, null]);
  frame2.load([6, null]);

  frame1.oneFrameForward(frame2);
  expect(frame1.getScore()).toBe(16);
});

//----------------------------------------------------
test('test twoFramesForward', () => {
  let frame1 = new Frame();
  let frame2 = new Frame();
  let frame3 = new Frame();

  frame1.load([7, 3]);
  frame2.load([6, 3]);
  frame3.load([10, null]);

  frame1.oneFrameForward(frame2);
  frame1.twoFramesForward(frame3);

  expect(frame1.getScore()).toBe(16);

  frame1.load([10, null]);
  frame2.load([6, 3]);
  frame3.load([10, null]);

  frame1.oneFrameForward(frame2);
  frame1.twoFramesForward(frame3);

  expect(frame1.getScore()).toBe(19);

  frame1.load([10, null]);
  frame2.load([10, null]);
  frame3.load([3, null]);

  frame1.oneFrameForward(frame2);
  frame1.twoFramesForward(frame3);

  expect(frame1.getScore()).toBe(23);
});