# Chaingotech - Game of bowling

![NodeJS](resources/nodejs_logo.png)

## Dependencias

- NodeJS
- JEST

## Arrancar aplicación en local

1. Instalar paquetes necesarios

        npm install

2. Ejecutar script principal

        npm start

3. Ejecutar tests

        npm test