'use strict';

const Game = require('./class/game');

const example = [
  [1, 2],
  [10, null],
  [5, 4],
  [7, 3],
  [10, null],
  [10, null],
  [1, 4],
  [6, 2],
  [7, 3],
  [10, 3, 7]
];

let game = new Game();
game.load(example);
game.results();