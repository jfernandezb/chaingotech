'use strict';

class Frame {
  constructor() {
    this.first_roll = null;
    this.second_roll = null;
    this.extra_roll = null;
    this.score = null;
    this.frameForward = null
  }

  load(pins) {
    this.first_roll = pins[0] || 0;
    this.second_roll = pins[1] || 0;
    this.extra_roll = pins[2] ||  0;
    this.extra_second_roll = pins[3] || 0;
    this.score = this.first_roll + this.second_roll + this.extra_roll;
    this.frameForward = null;
  }

  oneFrameForward(frame) {
    if (this.isStrike()) {
      this.score += frame.first_roll;
      this.score += frame.second_roll;
      this.frameForward = frame;
    } else if (this.isSpare()) {
      this.score += frame.first_roll;
    }
  }

  twoFramesForward(frame) {
    if (this.isStrike() && this.frameForward.isStrike()) {
      this.score += frame.first_roll;
    }
  }

  getFirstRoll() {
    return this.first_roll;
  }

  getSecondRoll() {
    return this.second_roll;
  }

  getScore() {
    return this.score;
  }

  isStrike() {
    return this.first_roll == 10
  }

  isSpare() {
    return this.first_roll + this.second_roll == 10
  }
}

module.exports = Frame;