'use strict';

const Frame = require('./frame');

class Game {
  constructor() {
    this.frames = [];
    this.score = 0;
  }

  //-----------------------------------------------------------
  load(game_array) {
    let self = this;

    game_array.forEach(function (frame_array, index) {
      let frame = new Frame();
      frame.load(frame_array);

      self.frames.push(frame);

      let oneFrameBack = self.frames[index - 1]
      if (oneFrameBack) { oneFrameBack.oneFrameForward(frame); }

      let twoFramesBack = self.frames[index - 2]
      if (twoFramesBack) { twoFramesBack.twoFramesForward(frame); }
    });

    this.frames.forEach(function (frame) {
      self.score += frame.score;
    });
  }

  //-----------------------------------------------------------
  results() {
    console.log('Frames score:');
    this.frames.forEach(function (frame) {
      console.log(frame.score);
    });
    console.log('---');
    console.log(`Game score: ${this.score}`);
  }

  //-----------------------------------------------------------
  getFrames() {
    return this.frames;
  }

  //-----------------------------------------------------------
  getFrame(i) {
    return this.frames[i];
  }

  //-----------------------------------------------------------
  getScore() {
    return this.score;
  }
}

module.exports = Game;